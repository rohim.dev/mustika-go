package migrations

import (
	"database/sql"
	"fmt"
	"log"
	"mustika-go/config"
	"mustika-go/exception"

	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	_ "github.com/jackc/pgx/v4/stdlib"
)

func Run(conf config.Config) {
	postgre_host := conf.Get("POSTGRE_HOST")
	postgre_user := conf.Get("POSTGRE_USER")
	postgre_pass := conf.Get("POSTGRE_PASS")
	postgre_name := conf.Get("POSTGRE_DB")
	postgre_port := conf.Get("POSTGRE_PORT")

	url := fmt.Sprintf("postgres://%s:%s@%s:%s/%s?sslmode=disable", postgre_user, postgre_pass, postgre_host, postgre_port, postgre_name)
	db, err := sql.Open("postgres", url)
	if err != nil {
		exception.PanicAndLog(exception.ServerError{
			Message: fmt.Sprintf("[Migrate.OpenDB] %s", err.Error()),
		})
	}

	driver, err := postgres.WithInstance(db, &postgres.Config{})
	if err != nil {
		exception.PanicAndLog(exception.ServerError{
			Message: fmt.Sprintf("[Migrate.WithInstance] %s", err.Error()),
		})
	}

	m, err := migrate.NewWithDatabaseInstance(
		"file://migrations/db",
		"postgres", driver)
	if err != nil {
		exception.PanicAndLog(exception.ServerError{
			Message: fmt.Sprintf("[Migrate.MigrateInstance] %s", err.Error()),
		})
	}

	if err := m.Up(); err != nil && err != migrate.ErrNoChange {
		exception.PanicAndLog(exception.ServerError{
			Message: fmt.Sprintf("[Migrate.RunMigrations] %s", err.Error()),
		})
	}

	log.Println("Migrations run successfully")
}
