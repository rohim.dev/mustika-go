package utils

import (
	"time"
)

type WebResponse struct {
	Code    int         `json:"code"`
	Status  string      `json:"status"`
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}

type WebArrayResponse struct {
	Code     int              `json:"code"`
	Status   string           `json:"status"`
	Message  string           `json:"message"`
	Data     interface{}      `json:"data"`
	Manifest ManifestResponse `json:"paginate"`
}

type ManifestResponse struct {
	Page  uint `json:"page"`
	Limit uint `json:"limit"`
	Total uint `json:"total"`
}

type RequestHeader struct {
	XUserAgent string `reqHeader:"user-agent"`
}

type Default struct {
	CreatedAt time.Time  `db:"created_at"`
	UpdatedAt time.Time  `db:"updated_at"`
	DeletedAt *time.Time `db:"deleted_at"`
}

type Pagination struct {
	Page  uint
	Limit uint
	Total uint `db:"pagination_total"`
}

type SortBy struct {
	Fieldname string
	Value     string
}

type GeneralEntity struct {
	Page   uint
	Limit  uint
	Search string
	Sort   string
}

type GeneralRequest struct {
	Page   string `query:"page"`
	Limit  string `query:"limit"`
	Search string `query:"search"`
	Sort   string `query:"sort"`
}
