package utils

import (
	"fmt"
	"math"
	"time"
)

func Now() time.Time {
	loc, _ := time.LoadLocation("Asia/Jakarta")

	// set timezone,
	now := time.Now().In(loc)
	return now
}

// func JakartaTime() *time.Location {
// 	jakartaTime, _ := time.LoadLocation("Asia/Jakarta")
// 	return jakartaTime
// }

func JakartaTime(t time.Time) time.Time {
	loc, err := time.LoadLocation("Asia/Jakarta")
	if err != nil {
		return t.UTC()
	}

	return t.UTC().In(loc)
}

var countryTz = map[string]string{
	"ina": "Asia/Jakarta",
}

func TimeIn(name string, timex time.Time) time.Time {
	loc, err := time.LoadLocation(countryTz[name])
	if err != nil {
		panic(err)
	}
	return timex.In(loc)
}

type Weekday int
type Month int

var days = [...]string{"Senin", "Selasa", "Rabo", "Kamis", "Jumat", "Sabtu", "Minggu"}
var months = [...]string{"Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"}

func (m Weekday) String() string { return days[m] }
func (m Month) String() string   { return months[m-1] }

func ParseDate(date string, parse string) (time.Time, string, string, string) {
	if parse == "" {
		parse = "2006-01-02 15:04:05"
	}
	t, err := time.Parse(parse, date)
	if err != nil {
		panic(err)
	}
	parseMonth := Month(int(t.Month()))
	year := t.Year()
	month := parseMonth.String()
	day := t.Day()
	parseWekkday := Weekday(int(t.Weekday()))
	weekday := parseWekkday.String()
	time := t.Format("15:04:05")

	return t, weekday, fmt.Sprintf("%d %s %d", day, month, year), time
}

func ConvertDateTime2TimeAgo(date time.Time) string {
	currentTime := time.Now()
	sub := currentTime.Sub(date)
	differentMilliseconds := int(sub.Milliseconds()) / 1000
	seconds := math.Floor(float64(differentMilliseconds))

	interval := math.Floor(seconds / 31536000)

	if interval > 1 {
		return fmt.Sprintf("%d tahun lalu", int(interval))
	}

	interval = math.Floor(seconds / 2592000)
	if interval > 1 {
		return fmt.Sprintf("%d bulan lalu", int(interval))
	}

	interval = math.Floor(seconds / 86400)
	if interval > 1 {
		return fmt.Sprintf("%d hari lalu", int(interval))
	}

	interval = math.Floor(seconds / 3600)
	if interval > 1 {
		return fmt.Sprintf("%d jam lalu", int(interval))
	}

	interval = math.Floor(seconds / 60)
	if interval > 1 {
		return fmt.Sprintf("%d menit lalu", int(interval))
	}

	if seconds < 30 {
		return "Baru saja"
	}

	return fmt.Sprintf("%d detik lalu", int(math.Floor(seconds)))
}

func ParseTimestamp(timestamp time.Time) string {
	return timestamp.Format("2006-01-02 15:04:05")
}
