package utils

import (
	"crypto/rand"
	"fmt"
	"math"
	"math/big"
	"net/http"
	"strings"
	"time"

	mathrand "math/rand"

	"github.com/microcosm-cc/bluemonday"
)

func GenerateRandomNumber(numberOfDigits int) (int, error) {
	maxLimit := int64(int(math.Pow10(numberOfDigits)) - 1)
	lowLimit := int(math.Pow10(numberOfDigits - 1))

	randomNumber, err := rand.Int(rand.Reader, big.NewInt(maxLimit))
	if err != nil {
		return 0, err
	}
	randomNumberInt := int(randomNumber.Int64())

	// Handling integers between 0, 10^(n-1) .. for n=4, handling cases between (0, 999)
	if randomNumberInt <= lowLimit {
		randomNumberInt += lowLimit
	}

	// Never likely to occur, kust for safe side.
	if randomNumberInt > int(maxLimit) {
		randomNumberInt = int(maxLimit)
	}
	return randomNumberInt, nil
}

func GenerateRandomString(length int, enableUppercase bool) string {
	charset := "abcdefghijklmnopqrstuvwxyz0123456789"
	if enableUppercase {
		charset += "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	}

	var seededRand *mathrand.Rand = mathrand.New(mathrand.NewSource(time.Now().UnixNano()))

	b := make([]byte, length)
	for i := range b {
		b[i] = charset[seededRand.Intn(len(charset))]
	}
	return string(b)
}

func PointerValue(value *string) string {
	if value != nil {
		return *value
	}
	return ""
}

func ReadUserIP(r *http.Request) string {
	IPAddress := r.Header.Get("X-Real-Ip")
	if IPAddress == "" {
		IPAddress = r.Header.Get("X-Forwarded-For")
	}
	if IPAddress == "" {
		IPAddress = r.RemoteAddr
	}
	return IPAddress
}

func ValidationPhone(phone string) string {
	if phone[0:1] == "0" {
		return fmt.Sprintf("62%s", phone[1:len(phone)])
	}
	return phone
}

func Html2String(html string) string {
	p := bluemonday.StripTagsPolicy()
	result := strings.ReplaceAll(p.Sanitize(html), "\n", " ")
	result = strings.TrimSpace(result)
	result = strings.Join(strings.Fields(result), " ")
	return result
}
