package controller

import (
	"mustika-go/exception"
	"mustika-go/src/auth/model"
	"mustika-go/src/auth/service"
	"mustika-go/utils"
	"net/http"

	"github.com/gofiber/fiber/v2"
)

type authController struct {
	authService service.AuthService
}

func NewAuthController(authService service.AuthService) *authController {
	return &authController{authService}
}

func (controller *authController) Route(app fiber.Router) {
	authV0 := app.Group("/api/v0")
	authV0.Post("/auth/login", controller.login)
	authV0.Post("/auth/register", controller.register)
}

// @Summary Register User
// @Accept  json
// @Tags Auth
// @Produce  json
// @Param   body     body    model.RegisterRequest     true        "Request body for register"
// @Success 200 {object} utils.WebResponse "Successful operation"
// @Router /api/v0/auth/register [post]
func (controller *authController) register(c *fiber.Ctx) error {
	var request model.RegisterRequest
	if err := c.BodyParser(&request); err != nil {
		exception.PanicIfNeeded(exception.ClientError{
			Message: err.Error(),
		})
	}

	result, err := controller.authService.Register(request)
	if err != nil {
		exception.PanicIfNeeded(exception.ClientError{
			Message: err.Error(),
		})
	}

	return c.JSON(utils.WebResponse{
		Code:    http.StatusOK,
		Status:  "success",
		Message: "register successfull",
		Data:    result,
	})
}

// @Summary Login User
// @Accept  json
// @Tags Auth
// @Produce  json
// @Param   body     body    model.LoginRequest     true        "Request body for register"
// @Success 200 {object} utils.WebResponse "Successful operation"
// @Router /api/v0/auth/login [post]
func (controller *authController) login(c *fiber.Ctx) error {
	var request model.LoginRequest
	if err := c.BodyParser(&request); err != nil {
		exception.PanicIfNeeded(exception.ClientError{
			Message: err.Error(),
		})
	}

	result, err := controller.authService.Login(request)
	if err != nil {
		exception.PanicIfNeeded(exception.ClientError{
			Message: err.Error(),
		})
	}

	return c.JSON(utils.WebResponse{
		Code:    http.StatusOK,
		Status:  "success",
		Message: "login successfull",
		Data:    result,
	})
}
