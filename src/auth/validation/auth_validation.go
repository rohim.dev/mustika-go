package validation

import (
	"errors"
	"mustika-go/src/auth/model"
	"net/mail"
	"strings"

	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/go-ozzo/ozzo-validation/v4/is"
)

type AuthValidation struct{}

func checkPassword(value interface{}) error {
	password, ok := value.(string)
	if !ok {
		return errors.New("is not a string")
	}

	hasUpper := false
	hasLower := false

	for _, ch := range password {
		if 'A' <= ch && ch <= 'Z' {
			hasUpper = true
		}
		if 'a' <= ch && ch <= 'z' {
			hasLower = true
		}
		if hasUpper && hasLower {
			return nil
		}
	}

	return errors.New("must contain at least one uppercase and one lowercase letter")
}

var (
	noSpaces = validation.NewStringRule(noSpacesValidator, "cannot contain spaces")
)

func noSpacesValidator(value string) bool {
	return !strings.Contains(value, " ")
}

func (authValidation *AuthValidation) RegisterValidation(request model.RegisterRequest) error {
	err := validation.ValidateStruct(&request,
		validation.Field(&request.Name, validation.Required.Error("name cannot be empty")),
		validation.Field(&request.Email, validation.Required, validation.When(authValidation.validationEmail(request.Email), validation.Required.Error("email not valid"))),
		validation.Field(&request.Username,
			validation.Required.Error("username cannot be empty"),
			validation.Length(4, 20),
			is.PrintableASCII,
			noSpaces,
		),
		validation.Field(&request.Password,
			validation.Required.Error("password cannot be empty"),
			validation.Length(6, 20),
			is.PrintableASCII,
			noSpaces,
		),
	)

	return err
}

func (authValidation *AuthValidation) LoginValidation(request model.LoginRequest) error {
	err := validation.ValidateStruct(&request,
		validation.Field(&request.Identity, validation.Required.Error("username or email cannot be empty")),
		validation.Field(&request.Password, validation.Required.Error("password cannot be empty")),
	)

	return err
}

func (authValidation *AuthValidation) validationEmail(email string) bool {
	_, err := mail.ParseAddress(email)
	return err == nil
}
