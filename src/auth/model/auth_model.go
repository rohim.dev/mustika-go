package model

import "time"

type RegisterRequest struct {
	Name     string `json:"name" example:"User"`
	Email    string `json:"email" example:"user@gmail.com"`
	Username string `json:"username" example:"user"`
	Password string `json:"password" example:"password"`
}

type LoginResponse struct {
	ID       string `json:"id"`
	Name     string `json:"name"`
	Email    string `json:"email"`
	Username string `json:"username"`
	Token    Token  `json:"token"`
}

type LoginRequest struct {
	Identity string `json:"identity" example:"user"`
	Password string `json:"password" example:"password"`
}

type Token struct {
	AccessToken string    `json:"access_token"`
	ExpiredAt   time.Time `json:"expired_at"`
}
