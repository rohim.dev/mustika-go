package enum

type ClickableType string

const (
	OTP_FLAG_REGISTRATION    string = "registration"
	OTP_FLAG_FORGET_PASSWORD string = "forget password"

	SEND_OTP_WITH_EMAIL string = "email"
	SEND_OTP_WITH_WA    string = "wa"

	NOTIFICATION_CLICKABLE_FOLLOW_USER ClickableType = "follow"
)
