package repository

import "mustika-go/src/auth/entity"

type AuthRepository interface {
	CreateAccount(request entity.User) error
	FindUserByUsernameOrEmail(identity string) (*entity.User, error)
	FindUserByUsername(email string) (*entity.User, error)
	FindUserByEmail(username string) (*entity.User, error)
}
