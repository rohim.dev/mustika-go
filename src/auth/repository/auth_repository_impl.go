package repository

import (
	"mustika-go/config"
	"mustika-go/src/auth/entity"
	"strings"

	sq "github.com/Masterminds/squirrel"
	"github.com/jmoiron/sqlx"
)

func NewAuthRepository(db *sqlx.DB) AuthRepository {
	return &authRepositoryImpl{db}
}

type authRepositoryImpl struct {
	db *sqlx.DB
}

func (r *authRepositoryImpl) CreateAccount(request entity.User) error {
	ctx, cancel := config.NewPostgreSqlxContext()
	defer cancel()

	createUser := sq.Insert("users").
		Columns("id", "name", "email", "username", "password").
		Values(request.ID, request.Name, request.Email, request.Username, request.Password).
		PlaceholderFormat(sq.Dollar)

	sql, args, err := createUser.ToSql()
	if err != nil {
		return err
	}

	_, err = r.db.ExecContext(ctx, sql, args...)
	if err != nil {
		return err
	}

	return nil
}

func (r *authRepositoryImpl) FindUserByUsernameOrEmail(identity string) (*entity.User, error) {
	ctx, cancel := config.NewPostgreSqlxContext()
	defer cancel()

	response := entity.User{}

	q := sq.Select().
		From("users a").
		Columns("a.id ,a.name , a.email, a.username, a.password").
		Where(sq.Or{
			sq.Eq{"a.username": identity},
			sq.Eq{"a.email": identity},
		}).PlaceholderFormat(sq.Dollar)

	sql, args, err := q.ToSql()
	if err != nil {
		return nil, err
	}

	if err := r.db.GetContext(ctx, &response, sql, args...); err != nil {
		if strings.Contains(err.Error(), "sql: no rows in result set") {
			return nil, nil
		}

		return nil, err
	}

	return &response, nil
}

func (r *authRepositoryImpl) FindUserByUsername(username string) (*entity.User, error) {
	ctx, cancel := config.NewPostgreSqlxContext()
	defer cancel()

	response := entity.User{}

	q := sq.Select().
		From("users a").
		Columns("a.id ,a.name , a.email, a.username, a.password").
		Where(sq.Eq{"a.username": username}).PlaceholderFormat(sq.Dollar)

	sql, args, err := q.ToSql()
	if err != nil {
		return nil, err
	}

	if err := r.db.GetContext(ctx, &response, sql, args...); err != nil {
		if strings.Contains(err.Error(), "sql: no rows in result set") {
			return nil, nil
		}

		return nil, err
	}

	return &response, nil
}

func (r *authRepositoryImpl) FindUserByEmail(email string) (*entity.User, error) {
	ctx, cancel := config.NewPostgreSqlxContext()
	defer cancel()

	response := entity.User{}

	q := sq.Select().
		From("users a").
		Columns("a.id ,a.name , a.email, a.username, a.password").
		Where(sq.Eq{"a.email": email}).PlaceholderFormat(sq.Dollar)

	sql, args, err := q.ToSql()
	if err != nil {
		return nil, err
	}

	if err := r.db.GetContext(ctx, &response, sql, args...); err != nil {
		if strings.Contains(err.Error(), "sql: no rows in result set") {
			return nil, nil
		}

		return nil, err
	}

	return &response, nil
}
