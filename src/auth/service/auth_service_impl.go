package service

import (
	"errors"
	"mustika-go/pkg/jwt"
	"mustika-go/src/auth/entity"
	"mustika-go/src/auth/model"
	"mustika-go/src/auth/repository"
	"mustika-go/src/auth/validation"
	"mustika-go/utils"
	"strings"
	"time"

	"github.com/google/uuid"
)

func NewAuthService(
	userRepo repository.AuthRepository,
	jwtProvider jwt.JwtProvider,
) AuthService {
	validator := new(validation.AuthValidation)
	return &authserviceImpl{userRepo, *validator, jwtProvider}
}

type authserviceImpl struct {
	userRepo    repository.AuthRepository
	validator   validation.AuthValidation
	jwtProvider jwt.JwtProvider
}

func (s *authserviceImpl) Login(request model.LoginRequest) (model.LoginResponse, error) {
	resp := model.LoginResponse{}
	if err := s.validator.LoginValidation(request); err != nil {
		return resp, err
	}

	user, err := s.userRepo.FindUserByUsernameOrEmail(request.Identity)
	if err != nil {
		if strings.Contains(err.Error(), "sql: no rows in result set") {
			return resp, errors.New("username atau email not found")
		}

		return resp, err
	}

	ok := utils.Verify(user.Password, request.Password)
	if !ok {
		return resp, errors.New("password not same")
	}

	jwtToken, _, expired, err := s.generateJwt(*user)
	if err != nil {
		return resp, errors.New("generate session token failed")
	}

	resp.ID = user.ID
	resp.Name = user.Name
	resp.Email = user.Email
	resp.Username = user.Username
	resp.Token = model.Token{
		AccessToken: jwtToken,
		ExpiredAt:   expired,
	}

	return resp, nil
}

func (s *authserviceImpl) Register(request model.RegisterRequest) (resp model.LoginResponse, err error) {
	resp = model.LoginResponse{}
	if err := s.validator.RegisterValidation(request); err != nil {
		return resp, err
	}

	exist, err := s.userRepo.FindUserByEmail(request.Email)
	if err != nil {
		return resp, err
	}
	if exist != nil {
		return resp, errors.New("email not available")
	}

	exist, err = s.userRepo.FindUserByEmail(request.Username)
	if err != nil {
		return resp, err
	}
	if exist != nil {
		return resp, errors.New("username not available")
	}

	pass, err := utils.Hash(request.Password)
	if err != nil {
		return resp, errors.New("hash password failed")
	}
	account := entity.User{
		ID:       uuid.NewString(),
		Name:     request.Name,
		Email:    request.Email,
		Username: request.Username,
		Password: pass,
	}
	err = s.userRepo.CreateAccount(account)
	if err != nil {
		return
	}

	jwtToken, _, expired, err := s.generateJwt(account)
	if err != nil {
		return resp, errors.New("generate session token failed")
	}

	resp.ID = account.ID
	resp.Name = account.Name
	resp.Username = account.Username
	resp.Email = account.Email
	resp.Token = model.Token{
		AccessToken: jwtToken,
		ExpiredAt:   expired,
	}

	return
}

func (s *authserviceImpl) generateJwt(user entity.User) (string, string, time.Time, error) {
	claims := jwt.JwtClaims{
		IssuedAt:  utils.Now(),
		ExpiresAt: utils.Now().AddDate(1, 0, 0),
		Id:        user.ID,
	}

	jwtToken, expiredAt, err := s.jwtProvider.Generate(claims)
	if err != nil {
		return "", "", time.Time{}, err
	}

	generate := utils.GenerateRandomString(50, false)

	return jwtToken, generate, expiredAt, nil
}
