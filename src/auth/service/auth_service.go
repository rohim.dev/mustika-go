package service

import (
	"mustika-go/src/auth/model"
)

type AuthService interface {
	Register(request model.RegisterRequest) (resp model.LoginResponse, err error)
	Login(request model.LoginRequest) (model.LoginResponse, error)
}
