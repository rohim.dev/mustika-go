package validation

import (
	"fmt"
	"mustika-go/src/order/model"

	validation "github.com/go-ozzo/ozzo-validation/v4"
)

type OrderValidation struct{}

func (orderValidation *OrderValidation) CreateOrderValidation(request model.CreateOrderRequest) error {
	err := validation.ValidateStruct(&request,
		validation.Field(&request.CustomerID, validation.Required.Error("customer cannot be empty")),
		validation.Field(&request.Product, validation.Required, validation.Length(1, 0)),
		validation.Field(&request.Product, validation.Each(validation.By(orderValidation.productValidation))),
	)

	return err
}

func (orderValidation *OrderValidation) productValidation(value interface{}) error {
	order, ok := value.(model.CreateOrderProductRequest)
	if !ok {
		return fmt.Errorf("invalid order")
	}
	return validation.ValidateStruct(&order,
		validation.Field(&order.Name, validation.Required, validation.Length(2, 100)),
		validation.Field(&order.Quantity, validation.Required, validation.Min(1)),
		validation.Field(&order.Price, validation.Required, validation.Min(0.0)),
	)
}

func (orderValidation *OrderValidation) UpdateOrderValidation(request model.UpdateOrderRequest) error {
	// var statusValue = []int{int(enum.STATUS_ORDER_PROCESS), int(enum.STATUS_ORDER_DONE), int(enum.STATUS_ORDER_CANCEL), int(enum.STATUS_ORDER_IN_DELIVERY)}
	err := validation.ValidateStruct(&request,
		validation.Field(&request.CustomerID, validation.Required.Error("customer cannot be empty")),
		// validation.Field(&request.Status, validation.Required, validation.In(statusValue)),
		validation.Field(&request.Product, validation.Required, validation.Length(1, 0)),
		validation.Field(&request.Product, validation.Each(validation.By(orderValidation.updateProductValidation))),
	)

	return err
}

func (orderValidation *OrderValidation) updateProductValidation(value interface{}) error {
	order, ok := value.(model.UpdateOrderProductRequest)
	if !ok {
		return fmt.Errorf("invalid order")
	}
	return validation.ValidateStruct(&order,
		validation.Field(&order.Name, validation.Required, validation.Length(2, 100)),
		validation.Field(&order.Quantity, validation.Required, validation.Min(1)),
		validation.Field(&order.Price, validation.Required, validation.Min(0.0)),
	)
}
