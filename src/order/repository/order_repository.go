package repository

import (
	"context"
	"mustika-go/src/order/entity"
	"mustika-go/utils"

	"github.com/jmoiron/sqlx"
)

type OrderRepository interface {
	GetOrders(req utils.GeneralEntity) (response []entity.Order, pagination utils.Pagination, err error)
	SaveOrder(request entity.Order) error
	UpdateOrder(orderId string, request entity.Order) error
	DeleteOrder(orderId string) error
	SaveOrderDetails(request []entity.OrderDetail) error
	FindOrderByID(orderID string) (*entity.Order, error)
	FindOrderDetailByOrderID(orderID string) ([]entity.OrderDetail, error)
	FindOrderDetailByID(orderDetailID string) (*entity.OrderDetail, error)

	// use to control database transaction from service
	WithTx(ctx context.Context) (OrderRepository, error)
	Tx() *sqlx.Tx
}
