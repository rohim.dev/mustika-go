package repository

import (
	"context"
	"database/sql"
	"fmt"
	"mustika-go/config"
	"mustika-go/src/order/entity"
	"mustika-go/utils"
	"strings"
	"time"

	sq "github.com/Masterminds/squirrel"
	"github.com/jmoiron/sqlx"
)

func NewOrderRepository(db *sqlx.DB) OrderRepository {
	return &orderRepositoryImpl{db, nil, false}
}

type orderRepositoryImpl struct {
	db     *sqlx.DB
	tx     *sqlx.Tx
	withTx bool
}

func (repo *orderRepositoryImpl) WithTx(ctx context.Context) (OrderRepository, error) {
	tx, err := repo.db.BeginTxx(ctx, &sql.TxOptions{})
	if err != nil {
		return repo, err
	}

	repo.withTx = true
	repo.tx = tx
	return repo, nil
}

func (repo *orderRepositoryImpl) Tx() *sqlx.Tx {
	repo.withTx = false
	return repo.tx
}

func (r *orderRepositoryImpl) GetOrders(req utils.GeneralEntity) (response []entity.Order, pagination utils.Pagination, err error) {
	ctx, cancel := config.NewPostgreSqlxContext()
	defer cancel()

	response = []entity.Order{}
	pagination = utils.Pagination{}

	statement := sq.Select().
		From("orders as a").
		Where(sq.Eq{
			"deleted_at": nil,
		}).
		PlaceholderFormat(sq.Dollar)

	if req.Search != "" {
		statement = statement.Where(sq.Or{
			sq.ILike{
				"a.customer_name": fmt.Sprintf("%%%s%%", req.Search),
			},
		})
	}

	//prepare for pagination
	pageStatement := statement.Columns("COUNT(1) as pagination_total")
	pageSql, pageArgs, err := pageStatement.ToSql()
	if err != nil {
		return
	}

	err = r.db.GetContext(ctx, &pagination, pageSql, pageArgs...)
	if err != nil {
		return
	}

	pagination.Page = req.Page
	pagination.Limit = req.Limit

	page := (req.Page - 1) * req.Limit
	statement = statement.Limit(uint64(req.Limit)).Offset(uint64(page))

	sortField := map[string]utils.SortBy{
		"id": {
			Fieldname: "a.id",
			Value:     "asc",
		},
		"-id": {
			Fieldname: "a.id",
			Value:     "desc",
		},
		"customer_name": {
			Fieldname: "a.customer_name",
			Value:     "asc",
		},
		"-customer_name": {
			Fieldname: "a.customer_name",
			Value:     "desc",
		},
		"total": {
			Fieldname: "a.total",
			Value:     "asc",
		},
		"-total": {
			Fieldname: "a.total",
			Value:     "desc",
		},
		"created_at": {
			Fieldname: "a.created_at",
			Value:     "asc",
		},
		"-created_at": {
			Fieldname: "a.created_at",
			Value:     "desc",
		},
	}

	if req.Sort == "" {
		req.Sort = "-created_at"
	}

	statement = statement.Columns("a.id, a.customer_id, a.customer_name, a.total, a.status, a.created_at, a.updated_at")
	statement = statement.OrderBy(fmt.Sprintf("%s %s", sortField[req.Sort].Fieldname, sortField[req.Sort].Value))
	sql, args, err := statement.ToSql()
	if err != nil {
		return
	}

	rows, err := r.db.QueryxContext(ctx, sql, args...)
	if err != nil {
		return
	}

	defer rows.Close()

	for rows.Next() {
		var item entity.Order
		err := rows.StructScan(&item)
		if err != nil {
			return response, pagination, err
		}

		response = append(response, item)
	}

	return response, pagination, nil
}

func (r *orderRepositoryImpl) SaveOrder(request entity.Order) error {
	ctx, cancel := config.NewPostgreSqlxContext()
	defer cancel()

	q := sq.Insert("orders").
		Columns("id", "customer_id", "customer_name", "total", "status").
		Values(request.ID, request.CustomerID, request.CustomerName, request.Total, request.Status).
		PlaceholderFormat(sq.Dollar)

	sql, args, err := q.ToSql()
	if err != nil {
		return err
	}

	if r.withTx {
		_, err = r.tx.ExecContext(ctx, sql, args...)
	} else {
		_, err = r.db.ExecContext(ctx, sql, args...)
	}

	return err
}

func (r *orderRepositoryImpl) UpdateOrder(orderID string, request entity.Order) error {
	ctx, cancel := config.NewPostgreSqlxContext()
	defer cancel()

	q := sq.Update("orders").
		Set("customer_id", request.CustomerID).
		Set("customer_name", request.CustomerName).
		Set("total", request.Total).
		Set("status", request.Status).
		Set("updated_at", utils.JakartaTime(time.Now())).
		Where(sq.Eq{
			"id": orderID,
		}).
		PlaceholderFormat(sq.Dollar)

	sql, args, err := q.ToSql()
	if err != nil {
		return err
	}

	if r.withTx {
		_, err = r.tx.ExecContext(ctx, sql, args...)
	} else {
		_, err = r.db.ExecContext(ctx, sql, args...)
	}

	return err
}

func (r *orderRepositoryImpl) DeleteOrder(orderID string) error {
	ctx, cancel := config.NewPostgreSqlxContext()
	defer cancel()

	q := sq.Update("orders").
		Set("updated_at", utils.JakartaTime(time.Now())).
		Set("deleted_at", utils.JakartaTime(time.Now())).
		Where(sq.Eq{
			"id": orderID,
		}).
		PlaceholderFormat(sq.Dollar)

	sql, args, err := q.ToSql()
	if err != nil {
		return err
	}

	if r.withTx {
		_, err = r.tx.ExecContext(ctx, sql, args...)
	} else {
		_, err = r.db.ExecContext(ctx, sql, args...)
	}

	return err
}

func (r *orderRepositoryImpl) SaveOrderDetails(request []entity.OrderDetail) error {
	ctx, cancel := config.NewPostgreSqlxContext()
	defer cancel()

	statement := sq.Insert("order_details").
		Columns("id", "order_id", "product", "price", "quantity").
		Suffix("ON CONFLICT(id) DO UPDATE SET order_id=excluded.order_id,product=excluded.product,price=excluded.price, quantity=excluded.quantity, updated_at = now()").
		PlaceholderFormat(sq.Dollar)
	for _, v := range request {
		statement = statement.
			Values(v.ID, v.OrderID, v.Product, v.Price, v.Quantity).
			PlaceholderFormat(sq.Dollar)
	}

	sql, args, err := statement.ToSql()
	if err != nil {
		return err
	}

	if r.withTx {
		_, err = r.tx.ExecContext(ctx, sql, args...)
	} else {
		_, err = r.db.ExecContext(ctx, sql, args...)
	}

	return err
}

func (r *orderRepositoryImpl) FindOrderByID(orderID string) (*entity.Order, error) {
	ctx, cancel := config.NewPostgreSqlxContext()
	defer cancel()

	response := entity.Order{}

	q := sq.Select().
		From("orders a").
		Columns("a.id, a.customer_id, a.customer_name, a.total, a.status, a.created_at, a.updated_at, a.deleted_at").
		Where(sq.Eq{"a.id": orderID, "a.deleted_at": nil}).PlaceholderFormat(sq.Dollar)

	sql, args, err := q.ToSql()
	if err != nil {
		return nil, err
	}

	if err := r.db.GetContext(ctx, &response, sql, args...); err != nil {
		if strings.Contains(err.Error(), "sql: no rows in result set") {
			return nil, nil
		}

		return nil, err
	}

	return &response, nil
}

func (r *orderRepositoryImpl) FindOrderDetailByOrderID(orderID string) ([]entity.OrderDetail, error) {
	ctx, cancel := config.NewPostgreSqlxContext()
	defer cancel()

	response := []entity.OrderDetail{}

	q := sq.Select().
		From("order_details a").
		Columns("a.id, a.order_id, a.product, a.price, a.quantity, a.created_at, a.updated_at").
		Where(sq.Eq{"a.order_id": orderID, "a.deleted_at": nil}).PlaceholderFormat(sq.Dollar)

	sql, args, err := q.ToSql()
	if err != nil {
		return nil, err
	}

	rows, err := r.db.QueryxContext(ctx, sql, args...)
	if err != nil {
		return response, err
	}

	defer rows.Close()

	for rows.Next() {
		var b entity.OrderDetail
		err := rows.StructScan(&b)
		if err != nil {
			return response, err
		}
		response = append(response, b)
	}

	return response, nil
}

func (r *orderRepositoryImpl) FindOrderDetailByID(orderDetailID string) (*entity.OrderDetail, error) {
	ctx, cancel := config.NewPostgreSqlxContext()
	defer cancel()

	response := entity.OrderDetail{}

	q := sq.Select().
		From("order_details a").
		Columns("a.id, a.order_id, a.product, a.price, a.quantity, a.created_at, a.updated_at").
		Where(sq.Eq{"a.id": orderDetailID}).PlaceholderFormat(sq.Dollar)

	sql, args, err := q.ToSql()
	if err != nil {
		return nil, err
	}

	if err := r.db.GetContext(ctx, &response, sql, args...); err != nil {
		if strings.Contains(err.Error(), "sql: no rows in result set") {
			return nil, nil
		}

		return nil, err
	}

	return &response, nil
}
