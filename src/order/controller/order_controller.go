package controller

import (
	"fmt"
	"mustika-go/exception"
	middleware "mustika-go/middlewares"
	"mustika-go/src/order/model"
	"mustika-go/src/order/service"
	"mustika-go/utils"
	"net/http"

	"github.com/gofiber/fiber/v2"
)

type orderController struct {
	orderService service.OrderService
}

func NewOrderController(orderService service.OrderService) *orderController {
	return &orderController{orderService}
}

func (controller *orderController) Route(app fiber.Router) {
	orderV0 := app.Group("/api/v0/order")
	orderV0.Get("/", middleware.AuthMiddleware, controller.getOrders)
	orderV0.Post("/", middleware.AuthMiddleware, controller.createOrder)
	orderV0.Put("/:orderId", middleware.AuthMiddleware, controller.updateOrder)
	orderV0.Get("/:orderId", middleware.AuthMiddleware, controller.detailOrder)
	orderV0.Delete("/:orderId", middleware.AuthMiddleware, controller.deleteOrder)
}

// @Summary List of Order
// @Accept  json
// @Tags Order
// @Param   page     query    string     false        "page"
// @Param   limit     query    string     false        "limit"
// @Param   search     query    string     false        "search by customer name"
// @Param   sort     query    string     false       "sort" Enums(id,-id,customer_name,-customer_name,total,-total,created_at,-created_at)
// @Success 200 {object} utils.WebArrayResponse "Successful operation"
// @Router /api/v0/order [get]
// @Security authtoken
func (controller *orderController) getOrders(c *fiber.Ctx) error {
	q := utils.GeneralRequest{}
	err := c.QueryParser(&q)
	if err != nil {
		exception.PanicIfNeeded(exception.ServerError{
			Message: err.Error(),
		})
	}

	response, manifest, err := controller.orderService.GetOrders(q)
	if err != nil {
		exception.PanicIfNeeded(exception.ServerError{
			Message: err.Error(),
		})
	}

	return c.JSON(utils.WebArrayResponse{
		Code:     200,
		Status:   "success",
		Message:  "Successfull",
		Data:     response,
		Manifest: manifest,
	})
}

// @Summary Create Order
// @Accept  json
// @Tags Order
// @Produce  json
// @Param   body     body    model.CreateOrderRequest     true        "Request body for create order"
// @Success 200 {object} utils.WebResponse "Successful operation"
// @Router /api/v0/order [post]
// @Security authtoken
func (controller *orderController) createOrder(c *fiber.Ctx) error {
	var request model.CreateOrderRequest
	fmt.Println(request)
	if err := c.BodyParser(&request); err != nil {
		exception.PanicIfNeeded(exception.ClientError{
			Message: err.Error(),
		})
	}

	err := controller.orderService.Createorder(request)
	if err != nil {
		exception.PanicIfNeeded(exception.ClientError{
			Message: err.Error(),
		})
	}

	return c.JSON(utils.WebResponse{
		Code:    http.StatusOK,
		Status:  "success",
		Message: "create order successfull",
	})
}

// @Summary Update Order
// @Accept  json
// @Tags Order
// @Produce  json
// @Param   orderId     path    string     true        "id of order"
// @Param   body     body    model.UpdateOrderRequest     true        "Request body for edit order"
// @Success 200 {object} utils.WebResponse "Successful operation"
// @Router /api/v0/order/{orderId} [put]
// @Security authtoken
func (controller *orderController) updateOrder(c *fiber.Ctx) error {
	orderId := c.Params("orderId")
	var request model.UpdateOrderRequest
	if err := c.BodyParser(&request); err != nil {
		exception.PanicIfNeeded(exception.ClientError{
			Message: err.Error(),
		})
	}

	if orderId == "" {
		exception.PanicIfNeeded(exception.ClientError{
			Message: "order ID cannot be empty",
		})
	}

	err := controller.orderService.UpdateOrder(orderId, request)
	if err != nil {
		exception.PanicIfNeeded(exception.ClientError{
			Message: err.Error(),
		})
	}

	return c.JSON(utils.WebResponse{
		Code:    http.StatusOK,
		Status:  "success",
		Message: "order successfully updated",
	})
}

// @Summary Detail Order
// @Accept  json
// @Tags Order
// @Produce  json
// @Param   orderId     path    string     true        "id of order"
// @Success 200 {object} utils.WebResponse "Successful operation"
// @Router /api/v0/order/{orderId} [get]
// @Security authtoken
func (controller *orderController) detailOrder(c *fiber.Ctx) error {
	orderId := c.Params("orderId")

	if orderId == "" {
		exception.PanicIfNeeded(exception.ClientError{
			Message: "order ID cannot be empty",
		})
	}

	result, err := controller.orderService.GetOrder(orderId)
	if err != nil {
		exception.PanicIfNeeded(exception.ClientError{
			Message: err.Error(),
		})
	}

	return c.JSON(utils.WebResponse{
		Code:    http.StatusOK,
		Status:  "success",
		Message: "successfull get detail order",
		Data:    result,
	})
}

// @Summary Delete Order
// @Accept  json
// @Tags Order
// @Produce  json
// @Param   orderId     path    string     true        "id of order"
// @Success 200 {object} utils.WebResponse "Successful operation"
// @Router /api/v0/order/{orderId} [delete]
// @Security authtoken
func (controller *orderController) deleteOrder(c *fiber.Ctx) error {
	orderId := c.Params("orderId")

	if orderId == "" {
		exception.PanicIfNeeded(exception.ClientError{
			Message: "order ID cannot be empty",
		})
	}

	err := controller.orderService.DeleteOrder(orderId)
	if err != nil {
		exception.PanicIfNeeded(exception.ClientError{
			Message: err.Error(),
		})
	}

	return c.JSON(utils.WebResponse{
		Code:    http.StatusOK,
		Status:  "success",
		Message: "order successfull deleted",
	})
}
