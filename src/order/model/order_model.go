package model

// List Order
type OrderResponse struct {
	ID       string `json:"id"`
	Customer struct {
		ID   string `json:"id"`
		Name string `json:"name"`
	} `json:"customer"`
	Total     float64 `json:"total"`
	Status    string  `json:"status"`
	CreatedAt string  `json:"created_at"`
	UpdatedAt string  `json:"updated_at"`
}

// Detail Order
type DetailOrderResponse struct {
	ID       string `json:"id"`
	Customer struct {
		ID   string `json:"id"`
		Name string `json:"name"`
	} `json:"customer"`
	Orders    []DetailOrderProductRequest `json:"products"`
	Total     float64                     `json:"total"`
	Status    string                      `json:"status"`
	CreatedAt string                      `json:"created_at"`
	UpdatedAt string                      `json:"updated_at"`
}

type DetailOrderProductRequest struct {
	ID       string  `json:"id"`
	Name     string  `json:"name"`
	Quantity int64   `json:"quantity"`
	Price    float64 `json:"price"`
	SubTotal float64 `json:"subtotal"`
}

// Create Order
type CreateOrderRequest struct {
	CustomerID string                      `json:"customer_id"`
	Product    []CreateOrderProductRequest `json:"product"`
}

type CreateOrderProductRequest struct {
	Name     string  `json:"name"`
	Quantity int64   `json:"quantity"`
	Price    float64 `json:"price"`
}

// Update Order
type UpdateOrderRequest struct {
	CustomerID string                      `json:"customer_id"`
	Status     int                         `json:"status"`
	Product    []UpdateOrderProductRequest `json:"product"`
}

type UpdateOrderProductRequest struct {
	ID       string  `json:"id"`
	Name     string  `json:"name"`
	Quantity int64   `json:"quantity"`
	Price    float64 `json:"price"`
	IsNew    bool    `json:"is_new" default:"false"` // if value is true then nothing check on order detail by id
}
