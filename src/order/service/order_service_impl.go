package service

import (
	"errors"
	"fmt"
	"mustika-go/config"
	"mustika-go/exception"
	"mustika-go/src/customer/outboundx"
	"mustika-go/src/order/entity"
	"mustika-go/src/order/enum"
	"mustika-go/src/order/model"
	"mustika-go/src/order/repository"
	"mustika-go/src/order/validation"
	"mustika-go/utils"
	"strconv"
	"time"

	"github.com/google/uuid"
	"golang.org/x/sync/errgroup"
)

func NewOrderService(
	orderRepo repository.OrderRepository,
	customerOutbound outboundx.CustomerOutbound,
) OrderService {
	validator := validation.OrderValidation{}
	return &orderServiceImpl{orderRepo, &validator, customerOutbound}
}

type orderServiceImpl struct {
	orderRepo        repository.OrderRepository
	validator        *validation.OrderValidation
	customerOutbound outboundx.CustomerOutbound
}

func (s *orderServiceImpl) GetOrders(request utils.GeneralRequest) (response []model.OrderResponse, manifest utils.ManifestResponse, err error) {
	response = []model.OrderResponse{}
	manifest = utils.ManifestResponse{}

	if request.Page == "" {
		request.Page = "1"
	}

	if request.Limit == "" {
		request.Limit = "10"
	}

	u64page, err := strconv.ParseUint(request.Page, 10, 32)
	if err != nil {
		return response, manifest, err
	}
	u64limit, err := strconv.ParseUint(request.Limit, 10, 32)
	if err != nil {
		return response, manifest, err
	}

	req := utils.GeneralEntity{
		Page:   uint(u64page),
		Limit:  uint(u64limit),
		Search: request.Search,
		Sort:   request.Sort,
	}

	results, pagination, err := s.orderRepo.GetOrders(req)
	if err != nil {
		exception.PanicIfNeeded(exception.ClientError{
			Message: err.Error(),
		})
	}

	for _, v := range results {
		item := model.OrderResponse{
			ID: v.ID,
			Customer: struct {
				ID   string "json:\"id\""
				Name string "json:\"name\""
			}{v.CustomerID, v.CustomerName},
			Total:     v.Total,
			Status:    enum.StatusOrder(v.Status).Parse2String(),
			CreatedAt: v.CreatedAt.Format(time.DateTime),
			UpdatedAt: v.UpdatedAt.Format(time.DateTime),
		}

		response = append(response, item)

	}

	if len(response) == 0 {
		response = nil
	}

	manifest = utils.ManifestResponse(pagination)

	return response, manifest, nil
}

func (s *orderServiceImpl) Createorder(request model.CreateOrderRequest) error {
	ctx, cancel := config.NewDefaultContext()
	defer cancel()

	if err := s.validator.CreateOrderValidation(request); err != nil {
		return err
	}

	customer, err := s.customerOutbound.FindCustomerByID(request.CustomerID)
	if err != nil {
		return err
	}

	if customer == nil {
		return errors.New("customer not found")
	}

	order := entity.Order{
		ID:           uuid.NewString(),
		CustomerID:   customer.ID,
		CustomerName: customer.Name,
		Status:       int(enum.STATUS_ORDER_PROCESS),
	}

	orderDetails := []entity.OrderDetail{}
	for _, v := range request.Product {
		orderDetails = append(orderDetails, entity.OrderDetail{
			ID:       uuid.NewString(),
			OrderID:  order.ID,
			Product:  v.Name,
			Quantity: v.Quantity,
			Price:    v.Price,
		})
		order.Total += v.Price * float64(v.Quantity)
	}

	tx, err := s.orderRepo.WithTx(ctx)
	if err != nil {
		return err
	}

	if err := tx.SaveOrder(order); err != nil {
		tx.Tx().Rollback()
		return err
	}

	if err := tx.SaveOrderDetails(orderDetails); err != nil {
		tx.Tx().Rollback()
		return err
	}

	if err := tx.Tx().Commit(); err != nil {
		tx.Tx().Rollback()
		return err
	}

	return nil
}

func (s *orderServiceImpl) UpdateOrder(orderID string, request model.UpdateOrderRequest) error {
	ctx, cancel := config.NewDefaultContext()
	defer cancel()

	if err := s.validator.UpdateOrderValidation(request); err != nil {
		return err
	}

	checkOrder, err := s.orderRepo.FindOrderByID(orderID)
	if err != nil {
		return err
	}

	if checkOrder == nil {
		return errors.New("order not found")
	}

	customer, err := s.customerOutbound.FindCustomerByID(request.CustomerID)
	if err != nil {
		return err
	}

	if customer == nil {
		return errors.New("customer not found")
	}

	order := entity.Order{
		ID:           orderID,
		CustomerID:   customer.ID,
		CustomerName: customer.Name,
		Status:       request.Status,
	}

	eg := new(errgroup.Group)
	orderDetails := []entity.OrderDetail{}
	for _, v := range request.Product {
		if v.IsNew {
			orderDetails = append(orderDetails, entity.OrderDetail{
				ID:       uuid.NewString(),
				OrderID:  order.ID,
				Product:  v.Name,
				Quantity: v.Quantity,
				Price:    v.Price,
			})
			order.Total += v.Price * float64(v.Quantity)
		} else {
			v := v // store the result of looping in local variable
			eg.Go(func() error {
				checkDetail, err := s.orderRepo.FindOrderDetailByID(v.ID)
				if err != nil {
					return err
				}

				if checkDetail == nil {
					return fmt.Errorf("product %s not found in your order ", v.Name)
				}
				orderDetails = append(orderDetails, entity.OrderDetail{
					ID:       v.ID,
					OrderID:  order.ID,
					Product:  v.Name,
					Quantity: v.Quantity,
					Price:    v.Price,
				})
				order.Total += v.Price * float64(v.Quantity)
				return nil
			})
		}
	}

	if err := eg.Wait(); err != nil {
		return err
	}

	tx, err := s.orderRepo.WithTx(ctx)
	if err != nil {
		return err
	}

	if err := tx.UpdateOrder(orderID, order); err != nil {
		tx.Tx().Rollback()
		return err
	}

	if err := tx.SaveOrderDetails(orderDetails); err != nil {
		tx.Tx().Rollback()
		return err
	}

	if err := tx.Tx().Commit(); err != nil {
		tx.Tx().Rollback()
		return err
	}

	return nil
}

func (s *orderServiceImpl) GetOrder(orderID string) (*model.DetailOrderResponse, error) {
	if orderID == "" {
		return nil, errors.New("order ID cannot be empty")
	}

	order, err := s.orderRepo.FindOrderByID(orderID)
	if err != nil {
		return nil, err
	}

	if order == nil {
		return nil, errors.New("order not found")
	}

	orderDetails, err := s.orderRepo.FindOrderDetailByOrderID(orderID)
	if err != nil {
		return nil, err
	}

	details := []model.DetailOrderProductRequest{}
	for _, v := range orderDetails {
		details = append(details, model.DetailOrderProductRequest{
			ID:       v.ID,
			Name:     v.Product,
			Price:    v.Price,
			Quantity: v.Quantity,
			SubTotal: v.Price * float64(v.Quantity),
		})
	}
	response := model.DetailOrderResponse{
		ID: order.ID,
		Customer: struct {
			ID   string "json:\"id\""
			Name string "json:\"name\""
		}{order.CustomerID, order.CustomerName},
		Orders:    details,
		Total:     order.Total,
		Status:    enum.StatusOrder(order.Status).Parse2String(),
		CreatedAt: order.CreatedAt.Format(time.DateTime),
		UpdatedAt: order.UpdatedAt.Format(time.DateTime),
	}

	return &response, nil
}

func (s *orderServiceImpl) DeleteOrder(orderId string) error {
	findById, err := s.orderRepo.FindOrderByID(orderId)
	if err != nil {
		return err
	}

	if findById == nil {
		return errors.New("order not found")
	}

	if findById.DeletedAt != nil {
		return errors.New("order has been deleted")
	}

	// send request to repository
	if err := s.orderRepo.DeleteOrder(orderId); err != nil {
		return err
	}

	return nil
}
