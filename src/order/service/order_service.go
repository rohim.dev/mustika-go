package service

import (
	"mustika-go/src/order/model"
	"mustika-go/utils"
)

type OrderService interface {
	GetOrders(request utils.GeneralRequest) (response []model.OrderResponse, manifest utils.ManifestResponse, err error)
	Createorder(request model.CreateOrderRequest) error
	UpdateOrder(orderID string, request model.UpdateOrderRequest) error
	GetOrder(orderID string) (*model.DetailOrderResponse, error)
	DeleteOrder(orderId string) error
}
