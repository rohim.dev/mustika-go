package enum

type StatusOrder int

const (
	STATUS_ORDER_PROCESS     StatusOrder = 1
	STATUS_ORDER_IN_DELIVERY StatusOrder = 2
	STATUS_ORDER_DONE        StatusOrder = 3
	STATUS_ORDER_CANCEL      StatusOrder = 4
)

func (s StatusOrder) Parse2String() string {
	result := ""
	switch s {
	case STATUS_ORDER_PROCESS:
		result = "PROCESS"
	case STATUS_ORDER_IN_DELIVERY:
		result = "IN DELIVERY"
	case STATUS_ORDER_DONE:
		result = "DONE"
	case STATUS_ORDER_CANCEL:
		result = "CANCEL"
	default:
		result = ""
	}
	return result
}
