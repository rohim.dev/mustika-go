package entity

import "time"

type Order struct {
	ID           string     `db:"id"`
	CustomerID   string     `db:"customer_id"`
	CustomerName string     `db:"customer_name"`
	Total        float64    `db:"total"`
	Status       int        `db:"status"` // 1 = process, 2 = in delivery, 3 = done, 4 = cancel
	CreatedAt    time.Time  `db:"created_at"`
	UpdatedAt    time.Time  `db:"updated_at"`
	DeletedAt    *time.Time `db:"deleted_at"`
}

type OrderDetail struct {
	ID        string    `db:"id"`
	OrderID   string    `db:"order_id"`
	Product   string    `db:"product"`
	Quantity  int64     `db:"quantity"`
	Price     float64   `db:"price"`
	CreatedAt time.Time `db:"created_at"`
	UpdatedAt time.Time `db:"updated_at"`
}
