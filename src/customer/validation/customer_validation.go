package validation

import (
	"mustika-go/src/customer/model"
	"net/mail"

	validation "github.com/go-ozzo/ozzo-validation/v4"
)

type CustomerValidation struct{}

func (customerValidation *CustomerValidation) RegisterValidation(request model.CustomerRequest) error {
	err := validation.ValidateStruct(&request,
		validation.Field(&request.Name, validation.Required.Error("name cannot be empty")),
		validation.Field(&request.Email, validation.Required, validation.When(customerValidation.validationEmail(request.Email), validation.Required.Error("email not valid"))),
		validation.Field(&request.Phone, validation.Required.Error("phone cannot be empty")),
	)

	return err
}

func (customerValidation *CustomerValidation) validationEmail(email string) bool {
	_, err := mail.ParseAddress(email)
	return err == nil
}
