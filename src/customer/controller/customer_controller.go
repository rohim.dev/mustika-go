package controller

import (
	"mustika-go/exception"
	middleware "mustika-go/middlewares"
	"mustika-go/src/customer/model"
	"mustika-go/src/customer/service"
	"mustika-go/utils"
	"net/http"

	"github.com/gofiber/fiber/v2"
)

type customerController struct {
	customerService service.CustomerService
}

func NewCustomerController(customerService service.CustomerService) *customerController {
	return &customerController{customerService}
}

func (controller *customerController) Route(app fiber.Router) {
	customerV0 := app.Group("/api/v0/customer")
	customerV0.Get("/", middleware.AuthMiddleware, controller.getCustomers)
	customerV0.Post("/", middleware.AuthMiddleware, controller.addCustomer)
	customerV0.Put("/:customerId", middleware.AuthMiddleware, controller.updateCustomer)
	customerV0.Get("/:customerId", middleware.AuthMiddleware, controller.detailCustomer)
	customerV0.Delete("/:customerId", middleware.AuthMiddleware, controller.deleteCustomer)
}

// @Summary List of Customers
// @Accept  json
// @Tags Customer
// @Param   page     query    string     false        "page"
// @Param   limit     query    string     false        "limit"
// @Param   search     query    string     false        "search by name, email, address"
// @Param   sort     query    string     false       "sort" Enums(id,-id,name,-name,email,-email,address,-address,phonne,-phonne,created_at,-created_at)
// @Success 200 {object} utils.WebArrayResponse "Successful operation"
// @Router /api/v0/customer [get]
// @Security authtoken
func (controller *customerController) getCustomers(c *fiber.Ctx) error {
	q := utils.GeneralRequest{}
	err := c.QueryParser(&q)
	if err != nil {
		exception.PanicIfNeeded(exception.ServerError{
			Message: err.Error(),
		})
	}

	response, manifest, err := controller.customerService.GetCustomers(q)
	if err != nil {
		exception.PanicIfNeeded(exception.ServerError{
			Message: err.Error(),
		})
	}

	return c.JSON(utils.WebArrayResponse{
		Code:     200,
		Status:   "success",
		Message:  "Successfull",
		Data:     response,
		Manifest: manifest,
	})
}

// @Summary Add Customer
// @Accept  json
// @Tags Customer
// @Produce  json
// @Param   body     body    model.CustomerRequest     true        "Request body for add customer"
// @Success 200 {object} utils.WebResponse "Successful operation"
// @Router /api/v0/customer [post]
// @Security authtoken
func (controller *customerController) addCustomer(c *fiber.Ctx) error {
	var request model.CustomerRequest
	if err := c.BodyParser(&request); err != nil {
		exception.PanicIfNeeded(exception.ClientError{
			Message: err.Error(),
		})
	}

	err := controller.customerService.SaveCustomer(request)
	if err != nil {
		exception.PanicIfNeeded(exception.ClientError{
			Message: err.Error(),
		})
	}

	return c.JSON(utils.WebResponse{
		Code:    http.StatusOK,
		Status:  "success",
		Message: "customer successfully addedd",
	})
}

// @Summary Update Customer
// @Accept  json
// @Tags Customer
// @Produce  json
// @Param   customerId     path    string     true        "id of customer"
// @Param   body     body    model.CustomerRequest     true        "Request body for edit customer"
// @Success 200 {object} utils.WebResponse "Successful operation"
// @Router /api/v0/customer/{customerId} [put]
// @Security authtoken
func (controller *customerController) updateCustomer(c *fiber.Ctx) error {
	customerId := c.Params("customerId")
	var request model.CustomerRequest
	if err := c.BodyParser(&request); err != nil {
		exception.PanicIfNeeded(exception.ClientError{
			Message: err.Error(),
		})
	}

	if customerId == "" {
		exception.PanicIfNeeded(exception.ClientError{
			Message: "customer ID cannot be empty",
		})
	}

	err := controller.customerService.UpdateCustomer(customerId, request)
	if err != nil {
		exception.PanicIfNeeded(exception.ClientError{
			Message: err.Error(),
		})
	}

	return c.JSON(utils.WebResponse{
		Code:    http.StatusOK,
		Status:  "success",
		Message: "customer successfully updated",
	})
}

// @Summary Detail Customer
// @Accept  json
// @Tags Customer
// @Produce  json
// @Param   customerId     path    string     true        "id of customer"
// @Success 200 {object} utils.WebResponse "Successful operation"
// @Router /api/v0/customer/{customerId} [get]
// @Security authtoken
func (controller *customerController) detailCustomer(c *fiber.Ctx) error {
	customerId := c.Params("customerId")

	if customerId == "" {
		exception.PanicIfNeeded(exception.ClientError{
			Message: "customer ID cannot be empty",
		})
	}

	result, err := controller.customerService.GetCustomerByID(customerId)
	if err != nil {
		exception.PanicIfNeeded(exception.ClientError{
			Message: err.Error(),
		})
	}

	return c.JSON(utils.WebResponse{
		Code:    http.StatusOK,
		Status:  "success",
		Message: "successfull get detail customer",
		Data:    result,
	})
}

// @Summary Delete Customer
// @Accept  json
// @Tags Customer
// @Produce  json
// @Param   customerId     path    string     true        "id of customer"
// @Success 200 {object} utils.WebResponse "Successful operation"
// @Router /api/v0/customer/{customerId} [delete]
// @Security authtoken
func (controller *customerController) deleteCustomer(c *fiber.Ctx) error {
	customerId := c.Params("customerId")

	if customerId == "" {
		exception.PanicIfNeeded(exception.ClientError{
			Message: "customer ID cannot be empty",
		})
	}

	err := controller.customerService.DeleteCustomer(customerId)
	if err != nil {
		exception.PanicIfNeeded(exception.ClientError{
			Message: err.Error(),
		})
	}

	return c.JSON(utils.WebResponse{
		Code:    http.StatusOK,
		Status:  "success",
		Message: "customer successfull deleted",
	})
}
