package outboundx

import (
	"mustika-go/src/customer/entity"
)

type CustomerOutbound interface {
	FindCustomerByID(customerID string) (*entity.Customer, error)
}
