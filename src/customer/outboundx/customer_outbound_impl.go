package outboundx

import (
	"mustika-go/config"
	"mustika-go/src/customer/entity"
	"strings"

	sq "github.com/Masterminds/squirrel"
	"github.com/jmoiron/sqlx"
)

func NewCustomerOutbound(db *sqlx.DB) CustomerOutbound {
	return &customerOutboundImpl{db}
}

type customerOutboundImpl struct {
	db *sqlx.DB
}

func (r *customerOutboundImpl) FindCustomerByID(customerID string) (*entity.Customer, error) {
	ctx, cancel := config.NewPostgreSqlxContext()
	defer cancel()

	response := entity.Customer{}

	q := sq.Select().
		From("customers a").
		Columns("a.id ,a.name , a.email, a.address, a.phone").
		Where(sq.Eq{"a.id": customerID, "a.deleted_at": nil}).PlaceholderFormat(sq.Dollar)

	sql, args, err := q.ToSql()
	if err != nil {
		return nil, err
	}

	if err := r.db.GetContext(ctx, &response, sql, args...); err != nil {
		if strings.Contains(err.Error(), "sql: no rows in result set") {
			return nil, nil
		}

		return nil, err
	}

	return &response, nil
}
