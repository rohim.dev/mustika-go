package repository

import (
	"mustika-go/src/customer/entity"
	"mustika-go/utils"
)

type CustomerRepository interface {
	GetCustomers(req utils.GeneralEntity) (response []entity.Customer, pagination utils.Pagination, err error)
	FindCustomerByID(customerID string) (*entity.Customer, error)
	FindCustomerByEmail(email string) (*entity.Customer, error)
	FindCustomerByPhone(phone string) (*entity.Customer, error)
	SaveCustomer(request entity.Customer) error
	UpdateCustomer(customerId string, request entity.Customer) error
	DeleteCustomer(customerID string) error
}
