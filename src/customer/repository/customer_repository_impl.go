package repository

import (
	"fmt"
	"mustika-go/config"
	"mustika-go/src/customer/entity"
	"mustika-go/utils"
	"strings"
	"time"

	sq "github.com/Masterminds/squirrel"
	"github.com/jmoiron/sqlx"
)

func NewCustomerRepository(db *sqlx.DB) CustomerRepository {
	return &customerRepositoryImpl{db}
}

type customerRepositoryImpl struct {
	db *sqlx.DB
}

func (r *customerRepositoryImpl) GetCustomers(req utils.GeneralEntity) (response []entity.Customer, pagination utils.Pagination, err error) {
	ctx, cancel := config.NewPostgreSqlxContext()
	defer cancel()

	response = []entity.Customer{}
	pagination = utils.Pagination{}

	statement := sq.Select().
		From("customers as a").
		Where(sq.Eq{
			"deleted_at": nil,
		}).
		PlaceholderFormat(sq.Dollar)

	if req.Search != "" {
		statement = statement.Where(sq.Or{
			sq.ILike{
				"a.name": fmt.Sprintf("%%%s%%", req.Search),
			},
			sq.ILike{
				"a.email": fmt.Sprintf("%%%s%%", req.Search),
			},
			sq.ILike{
				"a.address": fmt.Sprintf("%%%s%%", req.Search),
			},
		})
	}

	//prepare for pagination
	pageStatement := statement.Columns("COUNT(1) as pagination_total")
	pageSql, pageArgs, err := pageStatement.ToSql()
	if err != nil {
		return
	}

	err = r.db.GetContext(ctx, &pagination, pageSql, pageArgs...)
	if err != nil {
		return
	}

	pagination.Page = req.Page
	pagination.Limit = req.Limit

	page := (req.Page - 1) * req.Limit
	statement = statement.Limit(uint64(req.Limit)).Offset(uint64(page))

	sortField := map[string]utils.SortBy{
		"id": {
			Fieldname: "a.id",
			Value:     "asc",
		},
		"-id": {
			Fieldname: "a.id",
			Value:     "desc",
		},
		"name": {
			Fieldname: "a.name",
			Value:     "asc",
		},
		"-name": {
			Fieldname: "a.name",
			Value:     "desc",
		},
		"email": {
			Fieldname: "a.email",
			Value:     "asc",
		},
		"-email": {
			Fieldname: "a.email",
			Value:     "desc",
		},
		"address": {
			Fieldname: "a.address",
			Value:     "asc",
		},
		"-address": {
			Fieldname: "a.address",
			Value:     "desc",
		},
		"phone": {
			Fieldname: "a.phone",
			Value:     "asc",
		},
		"-phone": {
			Fieldname: "a.phone",
			Value:     "desc",
		},
		"created_at": {
			Fieldname: "a.created_at",
			Value:     "asc",
		},
		"-created_at": {
			Fieldname: "a.created_at",
			Value:     "desc",
		},
	}

	if req.Sort == "" {
		req.Sort = "-created_at"
	}

	statement = statement.Columns("a.id, a.name, a.address, a.email, a.phone, a.created_at, a.updated_at")
	statement = statement.OrderBy(fmt.Sprintf("%s %s", sortField[req.Sort].Fieldname, sortField[req.Sort].Value))
	sql, args, err := statement.ToSql()
	if err != nil {
		return
	}

	rows, err := r.db.QueryxContext(ctx, sql, args...)
	if err != nil {
		return
	}

	defer rows.Close()

	for rows.Next() {
		var item entity.Customer
		err := rows.StructScan(&item)
		if err != nil {
			return response, pagination, err
		}

		response = append(response, item)
	}

	return response, pagination, nil
}

func (r *customerRepositoryImpl) FindCustomerByID(customerID string) (*entity.Customer, error) {
	ctx, cancel := config.NewPostgreSqlxContext()
	defer cancel()

	response := entity.Customer{}

	q := sq.Select().
		From("customers a").
		Columns("a.id ,a.name , a.email, a.address, a.phone").
		Where(sq.Eq{"a.id": customerID, "a.deleted_at": nil}).PlaceholderFormat(sq.Dollar)

	sql, args, err := q.ToSql()
	if err != nil {
		return nil, err
	}

	if err := r.db.GetContext(ctx, &response, sql, args...); err != nil {
		if strings.Contains(err.Error(), "sql: no rows in result set") {
			return nil, nil
		}

		return nil, err
	}

	return &response, nil
}

func (r *customerRepositoryImpl) SaveCustomer(request entity.Customer) error {
	ctx, cancel := config.NewPostgreSqlxContext()
	defer cancel()

	createUser := sq.Insert("customers").
		Columns("name", "email", "address", "phone").
		Values(request.Name, request.Email, request.Address, request.Phone).
		PlaceholderFormat(sq.Dollar)

	sql, args, err := createUser.ToSql()
	if err != nil {
		return err
	}

	_, err = r.db.ExecContext(ctx, sql, args...)
	if err != nil {
		return err
	}

	return nil
}

func (r *customerRepositoryImpl) UpdateCustomer(customerId string, request entity.Customer) error {
	ctx, cancel := config.NewPostgreSqlxContext()
	defer cancel()

	createUser := sq.Update("customers").
		Set("name", request.Name).
		Set("email", request.Email).
		Set("address", request.Address).
		Set("phone", request.Phone).
		Set("updated_at", utils.JakartaTime(time.Now())).
		Where(sq.Eq{"id": customerId}).
		PlaceholderFormat(sq.Dollar)

	sql, args, err := createUser.ToSql()
	if err != nil {
		return err
	}

	_, err = r.db.ExecContext(ctx, sql, args...)
	if err != nil {
		return err
	}

	return nil
}

func (r *customerRepositoryImpl) DeleteCustomer(customerID string) error {
	ctx, cancel := config.NewPostgreSqlxContext()
	defer cancel()

	createUser := sq.Update("customers").
		Set("deleted_at", utils.JakartaTime(time.Now())).
		Where(sq.Eq{"id": customerID}).
		PlaceholderFormat(sq.Dollar)

	sql, args, err := createUser.ToSql()
	if err != nil {
		return err
	}

	_, err = r.db.ExecContext(ctx, sql, args...)
	if err != nil {
		return err
	}

	return nil
}

func (r *customerRepositoryImpl) FindCustomerByEmail(email string) (*entity.Customer, error) {
	ctx, cancel := config.NewPostgreSqlxContext()
	defer cancel()

	response := entity.Customer{}

	q := sq.Select().
		From("customers a").
		Columns("a.id ,a.name , a.email, a.address, a.phone").
		Where(sq.Eq{"a.email": email, "a.deleted_at": nil}).PlaceholderFormat(sq.Dollar)

	sql, args, err := q.ToSql()
	if err != nil {
		return nil, err
	}

	if err := r.db.GetContext(ctx, &response, sql, args...); err != nil {
		if strings.Contains(err.Error(), "sql: no rows in result set") {
			return nil, nil
		}

		return nil, err
	}

	return &response, nil
}

func (r *customerRepositoryImpl) FindCustomerByPhone(phone string) (*entity.Customer, error) {
	ctx, cancel := config.NewPostgreSqlxContext()
	defer cancel()

	response := entity.Customer{}

	q := sq.Select().
		From("customers a").
		Columns("a.id ,a.name , a.email, a.address, a.phone").
		Where(sq.Eq{"a.phone": phone, "a.deleted_at": nil}).PlaceholderFormat(sq.Dollar)

	sql, args, err := q.ToSql()
	if err != nil {
		return nil, err
	}

	if err := r.db.GetContext(ctx, &response, sql, args...); err != nil {
		if strings.Contains(err.Error(), "sql: no rows in result set") {
			return nil, nil
		}

		return nil, err
	}

	return &response, nil
}
