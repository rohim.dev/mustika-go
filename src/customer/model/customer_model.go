package model

type CustomerResponse struct {
	ID        string `json:"id"`
	Name      string `json:"name"`
	Email     string `json:"email"`
	Address   string `json:"address"`
	Phone     string `json:"phone"`
	CreatedAt string `json:"created_at"`
	UpdatedAt string `json:"updated_at"`
}

type CustomerRequest struct {
	Name    string `json:"name" example:"customer"`
	Email   string `json:"email" example:"email@gmail.com"`
	Address string `json:"address" example:"silicon valley"`
	Phone   string `json:"phone" example:"6289"`
}
