package service

import (
	"errors"
	"mustika-go/exception"
	"mustika-go/src/customer/entity"
	"mustika-go/src/customer/model"
	"mustika-go/src/customer/repository"
	"mustika-go/src/customer/validation"
	"mustika-go/utils"
	"strconv"
	"time"
)

func NewCustomerService(
	customerRepo repository.CustomerRepository,
) CustomerService {
	validator := validation.CustomerValidation{}
	return &customerServiceImpl{customerRepo, &validator}
}

type customerServiceImpl struct {
	customerRepo repository.CustomerRepository
	validator    *validation.CustomerValidation
}

func (s *customerServiceImpl) GetCustomers(request utils.GeneralRequest) (response []model.CustomerResponse, manifest utils.ManifestResponse, err error) {
	response = []model.CustomerResponse{}
	manifest = utils.ManifestResponse{}

	if request.Page == "" {
		request.Page = "1"
	}

	if request.Limit == "" {
		request.Limit = "10"
	}

	u64page, err := strconv.ParseUint(request.Page, 10, 32)
	if err != nil {
		return response, manifest, err
	}
	u64limit, err := strconv.ParseUint(request.Limit, 10, 32)
	if err != nil {
		return response, manifest, err
	}

	req := utils.GeneralEntity{
		Page:   uint(u64page),
		Limit:  uint(u64limit),
		Search: request.Search,
		Sort:   request.Sort,
	}

	results, pagination, err := s.customerRepo.GetCustomers(req)
	if err != nil {
		exception.PanicIfNeeded(exception.ClientError{
			Message: err.Error(),
		})
	}

	for _, v := range results {
		item := model.CustomerResponse{
			ID:        v.ID,
			Name:      v.Name,
			Email:     v.Email,
			Phone:     v.Phone,
			Address:   v.Address,
			CreatedAt: v.CreatedAt.Format(time.DateTime),
			UpdatedAt: v.UpdatedAt.Format(time.DateTime),
		}

		response = append(response, item)

	}

	if len(response) == 0 {
		response = nil
	}

	manifest = utils.ManifestResponse(pagination)

	return response, manifest, nil
}

func (s *customerServiceImpl) GetCustomerByID(customerID string) (response model.CustomerResponse, err error) {
	response = model.CustomerResponse{}
	if customerID == "" {
		return response, errors.New("customer id cannot be empty")
	}

	result, err := s.customerRepo.FindCustomerByID(customerID)
	if err != nil {
		return response, err
	}

	if result == nil {
		return response, errors.New("product cannot be found")
	}

	response = model.CustomerResponse{
		ID:        result.ID,
		Name:      result.Name,
		Email:     result.Email,
		Phone:     result.Phone,
		Address:   result.Address,
		CreatedAt: result.CreatedAt.Format(time.DateTime),
		UpdatedAt: result.UpdatedAt.Format(time.DateTime),
	}

	return response, nil

}

func (s *customerServiceImpl) SaveCustomer(request model.CustomerRequest) error {
	if err := s.validator.RegisterValidation(request); err != nil {
		return err
	}

	exist, err := s.customerRepo.FindCustomerByEmail(request.Email)
	if err != nil {
		return err
	}

	if exist != nil {
		return errors.New("email not available")
	}

	exist, err = s.customerRepo.FindCustomerByPhone(request.Phone)
	if err != nil {
		return err
	}
	if exist != nil {
		return err
	}

	customer := entity.Customer{
		Name:    request.Name,
		Email:   request.Email,
		Phone:   request.Phone,
		Address: request.Address,
	}
	err = s.customerRepo.SaveCustomer(customer)
	if err != nil {
		return err
	}

	return nil
}

func (s *customerServiceImpl) UpdateCustomer(customerId string, request model.CustomerRequest) error {
	if err := s.validator.RegisterValidation(request); err != nil {
		return err
	}

	findById, err := s.customerRepo.FindCustomerByID(customerId)
	if err != nil {
		return err
	}

	if findById == nil {
		return errors.New("customer not found")
	}

	exist, err := s.customerRepo.FindCustomerByEmail(request.Email)
	if err != nil {
		return err
	}

	if exist != nil && exist.ID != customerId {
		return errors.New("email not available")
	}

	exist, err = s.customerRepo.FindCustomerByPhone(request.Phone)
	if err != nil {
		return err
	}
	if exist != nil && exist.ID != customerId {
		return err
	}

	customer := entity.Customer{
		Name:    request.Name,
		Email:   request.Email,
		Phone:   request.Phone,
		Address: request.Address,
	}

	err = s.customerRepo.UpdateCustomer(customerId, customer)
	if err != nil {
		return err
	}

	return nil
}

func (s *customerServiceImpl) DeleteCustomer(customerId string) error {
	findById, err := s.customerRepo.FindCustomerByID(customerId)
	if err != nil {
		return err
	}

	if findById == nil {
		return errors.New("customer not found")
	}

	if findById.DeletedAt != nil {
		return errors.New("customer has been deleted")
	}

	// check the count of order

	// send request to repository
	if err := s.customerRepo.DeleteCustomer(customerId); err != nil {
		return err
	}

	return nil
}
