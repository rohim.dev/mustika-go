package service

import (
	"mustika-go/src/customer/model"
	"mustika-go/utils"
)

type CustomerService interface {
	GetCustomers(request utils.GeneralRequest) (response []model.CustomerResponse, manifest utils.ManifestResponse, err error)
	GetCustomerByID(customerID string) (response model.CustomerResponse, err error)
	SaveCustomer(request model.CustomerRequest) error
	UpdateCustomer(customerId string, request model.CustomerRequest) error
	DeleteCustomer(customerId string) error
}
