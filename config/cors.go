package config

import "github.com/gofiber/fiber/v2/middleware/cors"

func NewCorsConfig() cors.Config {
	return cors.Config{
		AllowOrigins: "*",
		AllowHeaders: "Origin, Content-Type, Accept, Authorization",
	}
}
