package config

import (
	"github.com/golang-jwt/jwt/v5"
)

type JwtConfig struct {
	SigningMethod    *jwt.SigningMethodHMAC
	SigningSiganture []byte
}

func NewJwtConfig(config Config) JwtConfig {
	return JwtConfig{
		SigningMethod:    jwt.SigningMethodHS256,
		SigningSiganture: []byte(config.Get("JWT_SECRET_KEY")),
	}
}
