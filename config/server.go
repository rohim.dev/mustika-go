package config

type ServerConfig struct {
	Port string
	Env  string
}

func NewServerConfig(config Config) *ServerConfig {
	return &ServerConfig{
		Port: config.Get("SERVER_PORT"),
		Env:  config.Get("SERVER_ENV"),
	}
}
