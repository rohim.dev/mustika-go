package middleware

import (
	"fmt"
	"mustika-go/exception"
	"os"

	"github.com/gofiber/fiber/v2"
	"github.com/golang-jwt/jwt/v5"
)

func AuthMiddleware(c *fiber.Ctx) error {
	reqToken := c.Get("Authorization")

	if reqToken == "" {
		exception.PanicIfNeeded(exception.UnauthenticatedError{
			Message: "unauthorized",
		})
	}
	// handle access token
	claims, err := validateToken(reqToken)

	if err != nil {
		exception.PanicIfNeeded(exception.UnauthenticatedError{
			Message: "unauthorized",
		})
	}

	c.Locals("userID", claims["id"].(string))
	return c.Next()
}

func validateToken(signedToken string) (claims jwt.MapClaims, msg error) {
	token, err := jwt.Parse(signedToken, func(t *jwt.Token) (interface{}, error) {
		if _, ok := t.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unauthenticated")
		}

		return []byte(os.Getenv("JWT_SECRET_KEY")), nil
	})

	if err != nil {
		msg = err
		return
	}

	claims, ok := token.Claims.(jwt.MapClaims)
	if !ok {
		msg = err
		return
	}

	return claims, msg
}
