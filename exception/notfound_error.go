package exception

type NotFoundError struct {
	Message string
	Payload interface{}
}

func (e NotFoundError) Error() string {
	return e.Message
}

func (e NotFoundError) Data() any {
	return e.Payload
}
