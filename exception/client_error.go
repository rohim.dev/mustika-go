package exception

type ClientError struct {
	Message string
	Payload interface{}
}

func (err ClientError) Error() string {
	return err.Message
}

func (err ClientError) Data() any {
	return err.Payload
}
