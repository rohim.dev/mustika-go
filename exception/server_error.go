package exception

type ServerError struct {
	Message string
	Payload interface{}
}

func (err ServerError) Error() string {
	return err.Message
}

func (err ServerError) Data() any {
	return err.Payload
}
