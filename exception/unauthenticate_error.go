package exception

type UnauthenticatedError struct {
	Message string
	Payload interface{}
}

func (e UnauthenticatedError) Error() string {
	return e.Message
}

func (e UnauthenticatedError) Data() any {
	return e.Payload
}
