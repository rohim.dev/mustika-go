package exception

type ValidationError struct {
	Message string
	Payload interface{}
}

func (err ValidationError) Error() string {
	return err.Message
}

func (err ValidationError) Data() any {
	return err.Payload
}
