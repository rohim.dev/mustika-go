package firebase

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"time"

	firebase "firebase.google.com/go"
	"firebase.google.com/go/messaging"
)

type FcmClient interface {
	SubscribeToken(topic string, fcmId string) error
	SendNotification(token string, notification map[string]interface{}, data map[string]interface{}) error
	SendNotificationToTopic(topic string, notification messaging.Notification, data map[string]string) error
	SendNotificationToDevice(token string, notification messaging.Notification, data map[string]string) error
}

type fcmClientImpl struct {
	app       *firebase.App
	serverKey string
}

func NewFcm(serverKey string) FcmClient {
	return &fcmClientImpl{
		serverKey: serverKey,
	}
}

// subscribe token to tipic
func (f *fcmClientImpl) SubscribeToken(topic string, fcmId string) error {
	url := fmt.Sprintf("https://iid.googleapis.com/iid/v1/%s/rel/topics/%s", fcmId, topic)
	method := "POST"

	client := &http.Client{}
	req, err := http.NewRequest(method, url, nil)

	if err != nil {
		fmt.Println(err)
		return err
	}
	req.Header.Add("Authorization", fmt.Sprintf("key=%s", f.serverKey))

	res, err := client.Do(req)
	if err != nil {
		fmt.Println(err)
		return err
	}
	defer res.Body.Close()

	_, err = io.ReadAll(res.Body)
	if err != nil {
		fmt.Println(err)
		return err
	}
	return nil
}

// send notification
func (f *fcmClientImpl) SendNotification(token string, notification map[string]interface{}, data map[string]interface{}) error {
	url := "https://fcm.googleapis.com/fcm/send"
	method := "POST"
	values := map[string]interface{}{
		"to":                token,
		"notification":      notification,
		"data":              data,
		"content_available": true,
		"priority":          "high",
	}
	_, err := json.Marshal(values)
	if err != nil {
		return err
	}

	payload, _ := json.Marshal(values)
	client := &http.Client{}
	req, err := http.NewRequest(method, url, bytes.NewBuffer(payload))

	if err != nil {
		fmt.Println(err)
		return err
	}
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Authorization", fmt.Sprintf("key=%s", f.serverKey))

	res, err := client.Do(req)
	if err != nil {
		fmt.Println(err)
		return err
	}
	defer res.Body.Close()

	_, err = io.ReadAll(res.Body)
	if err != nil {
		fmt.Println(err)
		return err
	}
	return nil
}

// send notification by device
func (f *fcmClientImpl) SendNotificationToDevice(token string, notification messaging.Notification, data map[string]string) error {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	firebaseMessage, err := f.app.Messaging(ctx)
	if err != nil {
		return err
	}

	message := &messaging.Message{
		Notification: &notification,
		Data:         data,
		Token:        token,
	}

	_, err = firebaseMessage.Send(ctx, message)
	if err != nil {
		return err
	}

	return nil
}

// send notification by topic
func (f *fcmClientImpl) SendNotificationToTopic(topic string, notification messaging.Notification, data map[string]string) error {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	firebaseMessage, err := f.app.Messaging(ctx)
	if err != nil {
		return err
	}

	message := &messaging.Message{
		Notification: &notification,
		Data:         data,
		Topic:        topic,
	}

	_, err = firebaseMessage.Send(ctx, message)
	if err != nil {
		return err
	}

	return nil
}
