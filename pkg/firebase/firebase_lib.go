package firebase

import (
	"context"

	"cloud.google.com/go/firestore"
	firebase "firebase.google.com/go"
	"google.golang.org/api/option"
)

type FirebaseConfig struct {
	ProjectId              string
	ServiceAccount         string
	AuthorizationServerKey string
}

type FirebaseClient interface {
	Firestore() (*firestore.Client, error)
	Fcm() FcmClient
}

type firebaseClientImpl struct {
	config    *firebase.Config
	app       *firebase.App
	serverKey string
}

func New(config FirebaseConfig) (FirebaseClient, error) {
	ctx := context.Background()

	cfg := &firebase.Config{
		ProjectID: config.ProjectId,
	}

	sa := option.WithCredentialsFile(config.ServiceAccount)
	app, err := firebase.NewApp(ctx, cfg, sa)
	if err != nil {
		return nil, err
	}

	client := &firebaseClientImpl{
		config:    cfg,
		app:       app,
		serverKey: config.AuthorizationServerKey,
	}

	return client, nil
}

// GetFirestore implements FirebaseClient
func (f *firebaseClientImpl) Firestore() (*firestore.Client, error) {
	ctx := context.Background()

	cli, err := f.app.Firestore(ctx)
	if err != nil {
		return nil, err
	}

	return cli, nil
}

// Fcm implements FirebaseClient.
func (f *firebaseClientImpl) Fcm() FcmClient {
	return &fcmClientImpl {
		app: f.app,
		serverKey: f.serverKey,
	}
}
