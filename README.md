# Golang Project with Fiber Frameowkr

This guide outlines the steps to run a Golang with fiber framework, including database migration, seeders, running the server, and example API usage.

## Prerequisites

1. Golang
2. Fiber
3. Go Migrate
4. PostgreSQL
5. Docker

## Project Installation

1. Clone the project repository:

    ```bash
    git clone https://gitlab.com/rohim.dev/mustika-go.git
    cd test-actionpay
    ```

2. Install dependencies:

    ```bash
    go mod tidy
    ```

3. Create a `.env` file and adjust the configuration according to your environment:

    ```env
    SERVER_PORT=8001
    SERVER_ENV=development
    SERVER_SECRET=5c634d75b44ce45e74f3092a83c2907df2e9b6d70eb1f5e7313ce9d74f50f720

    POSTGRE_HOST=127.0.0.1
    POSTGRE_PORT=5432
    POSTGRE_USER=postgres
    POSTGRE_PASS=postgres
    POSTGRE_DB=test_mustika_go
    POSTGRE_SSL=disable
    POSTGRE_SSL_ROOT_CERT=global-bundle.pem
    POSTGRE_POOL_MIN=10
    POSTGRE_POOL_MAX=100
    POSTGRE_MAX_LIFE_TIME=60
    POSTGRE_MAX_IDLE=10

    JWT_SECRET_KEY=91568457825552af016cf12ec480c09885aab6b682cac7672fe61a6e9be9b6281c3f77909eaafb6dc36190a9188ac2111c3729a4f1a50986783e35a6dca634c1
    ```

## Running the Server Without Docker

1. Create a database in your locally postgres server with name `mustika_go`

2. Run the server in development mode:

    ```bash
    make server-start
    or# or
    go run main.go
    ```

    ```bash
    apps will be running in port 8001
    ```
3. Server with running with port http://localhost:8001

4. Health check on url http://localhost:8001/api/health-check

## Running the Server With Docker

1. Run the server in development mode:

    ```bash
    docker compose up
    ```
    or
    ```bash
    docker compose up -d
    ```

    ```bash
    apps will be running in port 8001
    ```
2. Server with running with port http://localhost:8001

3. Health check on url http://localhost:8001/api/health-check

## Documentation

For more detailed documentation, visit the [API DOCUMENTATION](http://localhost:8001/swagger/index.html).
