package main

import (
	"fmt"
	"log"
	"mustika-go/config"
	"mustika-go/exception"
	"mustika-go/migrations"
	"mustika-go/pkg/jwt"
	"mustika-go/utils"
	"os"
	"os/signal"
	"syscall"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/recover"

	fiberSwagger "github.com/swaggo/fiber-swagger"

	_ "mustika-go/docs"

	auth_ctrl "mustika-go/src/auth/controller"
	auth_repo "mustika-go/src/auth/repository"
	auth_svc "mustika-go/src/auth/service"

	customer_ctrl "mustika-go/src/customer/controller"
	customer_outbound "mustika-go/src/customer/outboundx"
	customer_repo "mustika-go/src/customer/repository"
	customer_svc "mustika-go/src/customer/service"

	order_ctrl "mustika-go/src/order/controller"
	order_repo "mustika-go/src/order/repository"
	order_svc "mustika-go/src/order/service"
)

// @title Docs API
// @description This is a documentation API.
// @version 1.0.1

// @schemes http https
// @securityDefinitions.apiKey authtoken
// @in header
// @name authorization

// @termsOfService http://swagger.io/terms/

// @contact.name API Support
// @contact.url http://www.swagger.io/support
// @contact.email support@swagger.io

// @license.name Apache 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html

// @host localhost:8001

func main() {
	// Setup Configuration
	configuration := config.New()
	serverConfig := config.NewServerConfig(configuration)
	jwtConfig := config.NewJwtConfig(configuration)

	// Initialize pkg
	utils.NewLogger()

	//Open DB
	postgre := config.NewPostgreSqlxDatabase(configuration)

	// Migration DB
	migrations.Run(configuration)

	jwtProvider := jwt.New(jwt.JwtProviderConfig{
		SigningMethod:    jwtConfig.SigningMethod,
		SigningSiganture: jwtConfig.SigningSiganture,
	})

	// outbounnd
	customerOutbound := customer_outbound.NewCustomerOutbound(postgre.DB)

	// Setup Repository
	authRepo := auth_repo.NewAuthRepository(postgre.DB)
	customerRepo := customer_repo.NewCustomerRepository(postgre.DB)
	orderRepo := order_repo.NewOrderRepository(postgre.DB)

	// Setup Service
	authService := auth_svc.NewAuthService(authRepo, jwtProvider)
	customerService := customer_svc.NewCustomerService(customerRepo)
	orderService := order_svc.NewOrderService(orderRepo, customerOutbound)

	// Setup Controller
	authCtrl := auth_ctrl.NewAuthController(authService)
	customerCtrl := customer_ctrl.NewCustomerController(customerService)
	orderCtrl := order_ctrl.NewOrderController(orderService)

	// Setup Fiber
	app := fiber.New(config.NewFiberConfig())
	app.Use(recover.New())
	app.Use(config.NewLogger(config.LoggerConfig{
		Logger: utils.Logger,
	}))

	// Setup Routing
	public := app.Group("/api")
	public.Get("/health-check", func(ctx *fiber.Ctx) error {
		return ctx.Status(200).JSON(map[string]interface{}{
			"code":   200,
			"status": "success",
			"data":   fmt.Sprintf("Version %s", configuration.Get("APP_VERSION")),
			"IP":     ctx.IP(),
			"IPs":    ctx.IPs(),
		})
	})

	app.Get("/swagger/*", fiberSwagger.WrapHandler)
	//Register Rest API Route
	authCtrl.Route(app)
	customerCtrl.Route(app)
	customerCtrl.Route(app)
	orderCtrl.Route(app)

	// Start App
	stop := make(chan os.Signal, 1)
	signal.Notify(stop, os.Interrupt, syscall.SIGTERM, syscall.SIGINT)
	go func() {
		err := app.Listen(fmt.Sprintf(":%s", serverConfig.Port))
		if err != nil {
			exception.PanicIfNeeded(err)
		}
	}()

	// graceful shutdown
	<-stop
	log.Println("Stopping server...")
	postgre.DB.Close()
}
