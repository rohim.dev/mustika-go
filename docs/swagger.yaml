definitions:
  model.CreateOrderProductRequest:
    properties:
      name:
        type: string
      price:
        type: number
      quantity:
        type: integer
    type: object
  model.CreateOrderRequest:
    properties:
      customer_id:
        type: string
      product:
        items:
          $ref: '#/definitions/model.CreateOrderProductRequest'
        type: array
    type: object
  model.CustomerRequest:
    properties:
      address:
        example: silicon valley
        type: string
      email:
        example: email@gmail.com
        type: string
      name:
        example: customer
        type: string
      phone:
        example: "6289"
        type: string
    type: object
  model.LoginRequest:
    properties:
      identity:
        example: user
        type: string
      password:
        example: password
        type: string
    type: object
  model.RegisterRequest:
    properties:
      email:
        example: user@gmail.com
        type: string
      name:
        example: User
        type: string
      password:
        example: password
        type: string
      username:
        example: user
        type: string
    type: object
  model.UpdateOrderProductRequest:
    properties:
      id:
        type: string
      is_new:
        default: false
        description: if value is true then nothing check on order detail by id
        type: boolean
      name:
        type: string
      price:
        type: number
      quantity:
        type: integer
    type: object
  model.UpdateOrderRequest:
    properties:
      customer_id:
        type: string
      product:
        items:
          $ref: '#/definitions/model.UpdateOrderProductRequest'
        type: array
      status:
        type: integer
    type: object
  utils.ManifestResponse:
    properties:
      limit:
        type: integer
      page:
        type: integer
      total:
        type: integer
    type: object
  utils.WebArrayResponse:
    properties:
      code:
        type: integer
      data: {}
      message:
        type: string
      paginate:
        $ref: '#/definitions/utils.ManifestResponse'
      status:
        type: string
    type: object
  utils.WebResponse:
    properties:
      code:
        type: integer
      data: {}
      message:
        type: string
      status:
        type: string
    type: object
host: localhost:8001
info:
  contact:
    email: support@swagger.io
    name: API Support
    url: http://www.swagger.io/support
  description: This is a documentation API.
  license:
    name: Apache 2.0
    url: http://www.apache.org/licenses/LICENSE-2.0.html
  termsOfService: http://swagger.io/terms/
  title: Docs API
  version: 1.0.1
paths:
  /api/v0/auth/login:
    post:
      consumes:
      - application/json
      parameters:
      - description: Request body for register
        in: body
        name: body
        required: true
        schema:
          $ref: '#/definitions/model.LoginRequest'
      produces:
      - application/json
      responses:
        "200":
          description: Successful operation
          schema:
            $ref: '#/definitions/utils.WebResponse'
      summary: Login User
      tags:
      - Auth
  /api/v0/auth/register:
    post:
      consumes:
      - application/json
      parameters:
      - description: Request body for register
        in: body
        name: body
        required: true
        schema:
          $ref: '#/definitions/model.RegisterRequest'
      produces:
      - application/json
      responses:
        "200":
          description: Successful operation
          schema:
            $ref: '#/definitions/utils.WebResponse'
      summary: Register User
      tags:
      - Auth
  /api/v0/customer:
    get:
      consumes:
      - application/json
      parameters:
      - description: page
        in: query
        name: page
        type: string
      - description: limit
        in: query
        name: limit
        type: string
      - description: search by name, email, address
        in: query
        name: search
        type: string
      - description: sort
        enum:
        - id
        - -id
        - name
        - -name
        - email
        - -email
        - address
        - -address
        - phonne
        - -phonne
        - created_at
        - -created_at
        in: query
        name: sort
        type: string
      responses:
        "200":
          description: Successful operation
          schema:
            $ref: '#/definitions/utils.WebArrayResponse'
      security:
      - authtoken: []
      summary: List of Customers
      tags:
      - Customer
    post:
      consumes:
      - application/json
      parameters:
      - description: Request body for add customer
        in: body
        name: body
        required: true
        schema:
          $ref: '#/definitions/model.CustomerRequest'
      produces:
      - application/json
      responses:
        "200":
          description: Successful operation
          schema:
            $ref: '#/definitions/utils.WebResponse'
      security:
      - authtoken: []
      summary: Add Customer
      tags:
      - Customer
  /api/v0/customer/{customerId}:
    delete:
      consumes:
      - application/json
      parameters:
      - description: id of customer
        in: path
        name: customerId
        required: true
        type: string
      produces:
      - application/json
      responses:
        "200":
          description: Successful operation
          schema:
            $ref: '#/definitions/utils.WebResponse'
      security:
      - authtoken: []
      summary: Delete Customer
      tags:
      - Customer
    get:
      consumes:
      - application/json
      parameters:
      - description: id of customer
        in: path
        name: customerId
        required: true
        type: string
      produces:
      - application/json
      responses:
        "200":
          description: Successful operation
          schema:
            $ref: '#/definitions/utils.WebResponse'
      security:
      - authtoken: []
      summary: Detail Customer
      tags:
      - Customer
    put:
      consumes:
      - application/json
      parameters:
      - description: id of customer
        in: path
        name: customerId
        required: true
        type: string
      - description: Request body for edit customer
        in: body
        name: body
        required: true
        schema:
          $ref: '#/definitions/model.CustomerRequest'
      produces:
      - application/json
      responses:
        "200":
          description: Successful operation
          schema:
            $ref: '#/definitions/utils.WebResponse'
      security:
      - authtoken: []
      summary: Update Customer
      tags:
      - Customer
  /api/v0/order:
    get:
      consumes:
      - application/json
      parameters:
      - description: page
        in: query
        name: page
        type: string
      - description: limit
        in: query
        name: limit
        type: string
      - description: search by customer name
        in: query
        name: search
        type: string
      - description: sort
        enum:
        - id
        - -id
        - customer_name
        - -customer_name
        - total
        - -total
        - created_at
        - -created_at
        in: query
        name: sort
        type: string
      responses:
        "200":
          description: Successful operation
          schema:
            $ref: '#/definitions/utils.WebArrayResponse'
      security:
      - authtoken: []
      summary: List of Order
      tags:
      - Order
    post:
      consumes:
      - application/json
      parameters:
      - description: Request body for create order
        in: body
        name: body
        required: true
        schema:
          $ref: '#/definitions/model.CreateOrderRequest'
      produces:
      - application/json
      responses:
        "200":
          description: Successful operation
          schema:
            $ref: '#/definitions/utils.WebResponse'
      security:
      - authtoken: []
      summary: Create Order
      tags:
      - Order
  /api/v0/order/{orderId}:
    delete:
      consumes:
      - application/json
      parameters:
      - description: id of order
        in: path
        name: orderId
        required: true
        type: string
      produces:
      - application/json
      responses:
        "200":
          description: Successful operation
          schema:
            $ref: '#/definitions/utils.WebResponse'
      security:
      - authtoken: []
      summary: Delete Order
      tags:
      - Order
    get:
      consumes:
      - application/json
      parameters:
      - description: id of order
        in: path
        name: orderId
        required: true
        type: string
      produces:
      - application/json
      responses:
        "200":
          description: Successful operation
          schema:
            $ref: '#/definitions/utils.WebResponse'
      security:
      - authtoken: []
      summary: Detail Order
      tags:
      - Order
    put:
      consumes:
      - application/json
      parameters:
      - description: id of order
        in: path
        name: orderId
        required: true
        type: string
      - description: Request body for edit order
        in: body
        name: body
        required: true
        schema:
          $ref: '#/definitions/model.UpdateOrderRequest'
      produces:
      - application/json
      responses:
        "200":
          description: Successful operation
          schema:
            $ref: '#/definitions/utils.WebResponse'
      security:
      - authtoken: []
      summary: Update Order
      tags:
      - Order
schemes:
- http
- https
securityDefinitions:
  authtoken:
    in: header
    name: authorization
    type: apiKey
swagger: "2.0"
