FROM golang:1.21-alpine

RUN apk add git

WORKDIR /app

COPY . .

RUN go get .

RUN go build -o mustika-go

EXPOSE 8001

CMD [ "./mustika-go" ]
